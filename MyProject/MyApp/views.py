from django.shortcuts import render

# Create your views here.


def index(request):  # Vista Inicio

    return render(request, 'index.html')


def manga(request):  # Vista Manga

    return render(request, 'manga.html')


def form(request):  # Vista formulario

    return render(request, 'form.html')


def base(request):

    return render(request, 'base.html')
