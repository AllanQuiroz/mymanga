from django.contrib import admin
from django.urls import path
from MyApp.views import index, manga, form, base

urlpatterns = [
    path('admin/', admin.site.urls),
    path('index/', index),
    path('manga/', manga),
    path('form/', form),
    path('base/', base)
]
